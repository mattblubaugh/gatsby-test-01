import axios from "axios";
import config from "../../config";

// env vars must be loaded again for netlify functions
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

exports.handler = function(event, context, callback) {
  const apiRoot = "https://api.unsplash.com";
  const accessKey = process.env.UNSPLASH_ACCESS_KEY || config.accessKey;

  const endpoint = `${apiRoot}/photos/random?client_id=${accessKey}&count=${10}&collections='3816141,1154337,1254279'`;

  axios
    .get(endpoint)
    .then(result => {
      callback(null, {
        statusCode: 200,
        body: JSON.stringify({
          images: result.data,
        }),
      });
    })
    .catch(error => {
      console.error(error);
    });
};
