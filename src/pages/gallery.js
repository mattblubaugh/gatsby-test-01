import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import InfiniteImages from "../components/InfiniteImages";

const Gallery = () => {
  return (
    <Layout>
      <SEO title="Gallery" />
      <h1 className="is-size-3">Images from unsplash</h1>
      <p style={{ marginBottom: "5%" }}>
        Infinite scroll effect with new images loaded as you scroll down the
        page.
      </p>
      <InfiniteImages />
    </Layout>
  );
};

export default Gallery;
