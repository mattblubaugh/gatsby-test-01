import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
// import Image from "../components/image"
import SEO from "../components/seo"
import "bulma/css/bulma.min.css"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <div className="has-text-centered" style={{ marginTop: "20%" }}>
      <h1 className="is-size-2">Gatsby Test</h1>
      <p className="is-size-5" style={{ marginTop: "2%" }}>
        Ruff bark fido doggie puppy paws playtime toys bone chewing roughhousing
        growl pounce sniff
      </p>
      <button className="button is-dark is-large" style={{ marginTop: "10%" }}>
        <Link to="/gallery" className="has-text-white">
          Go to gallery
        </Link>
      </button>
    </div>
  </Layout>
)

export default IndexPage
